import { BaseApi } from "../util/api"
import { Library } from "../model"

export class LibraryApi extends BaseApi {
  createLibrary = async (userId: string, payload: Partial<Library>): Promise<Library> => {
    return this.respondOrFail(await this.post(`/users/${userId}/libraries`, payload))
  }

  deleteLibrary = async (userId: string, libraryId: string): Promise<{ count: number }> => {
    return this.respondOrFail(await this.delete(`/users/${userId}/libraries??filter[where][id]=${libraryId}`))
  }
}
