import { Password } from "../util/password"

import encoder from "../util/encoder"
import { BaseApi } from "../util/api"

export class AuthApi extends BaseApi {
  login = async (email: string, password: Password): Promise<{ token: string }> => {
    const payload = { email, password: encoder.bytesToStr(password.value()) }
    return this.respondOrFail(await this.post("/login", payload))
  }
}
