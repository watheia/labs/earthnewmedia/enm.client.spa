import { BaseApi } from "../util/api"
import { User } from "../model"
import { Password } from "../util/password"
import encoder from "../util/encoder"

export class UserApi extends BaseApi {
  me = async (): Promise<User> => {
    return this.respondOrFail(await this.get("/users/me"))
  }

  findBySlug = async (userSlug: string): Promise<User> => {
    return this.respondOrFail(await this.get(`/users/${userSlug}`))
  }

  /**
   * Returns a valid JWT token after member registration
   */
  create = async (email: string, userName: string, password: Password): Promise<{ token: string }> => {
    const payload = { email, userName, password: encoder.bytesToStr(password.value()) }
    return this.respondOrFail(await this.post("/users", payload))
  }
}
