enum Verbs {
  DELETE = "DELETE",
  GET = "GET",
  POST = "POST",
  PUT = "PUT",
  UPDATE = "UPDATE"
}

export class BaseApi {
  constructor(gateway: string) {
    this.gateway = gateway
    this.token = null
  }

  gateway: string

  token: string | null

  async delete(resource: string): Promise<any> {
    return await fetch(`${this.gateway}${resource}`, {
      method: Verbs.DELETE,
      headers: this.headers()
    })
  }

  async get(path: string): Promise<any> {
    return await fetch(`${this.gateway}${path}`, {
      method: Verbs.GET,
      headers: this.headers()
    })
  }

  async post(path: string, payload: any): Promise<any> {
    return await fetch(`${this.gateway}${path}`, {
      body: JSON.stringify(payload),
      method: Verbs.POST,
      headers: this.headers()
    })
  }

  async put(resource: string, payload: any): Promise<any> {
    return await fetch(`${this.gateway}${resource}`, {
      method: Verbs.PUT,
      body: JSON.stringify(payload),
      headers: this.headers()
    })
  }

  async update(resource: string, payload: any): Promise<any> {
    return await fetch(`${this.gateway}${resource}`, {
      method: Verbs.UPDATE,
      body: JSON.stringify(payload),
      headers: this.headers()
    })
  }

  protected async respondOrFail(response: Response): Promise<any> {
    if (response.ok) {
      return response.json()
    } else {
      throw new Error(response.statusText)
    }
  }

  protected headers(): { [key: string]: string } {
    if (this.token) {
      return {
        Authorization: `Bearer ${this.token}`,
        "Content-Type": "application/json; charset=utf-8"
      }
    }

    return { "Content-Type": "application/json; charset=utf-8" }
  }
}
