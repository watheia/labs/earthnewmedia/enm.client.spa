import { TextEncoder } from "text-encoding"

function strToBytes(val: string): Uint8Array {
  return new TextEncoder().encode(val)
}

function bytesToStr(val: Uint8Array, encoding = "UTF-8"): string {
  return new TextDecoder(encoding).decode(val)
}

export default {
  strToBytes,
  bytesToStr
}
