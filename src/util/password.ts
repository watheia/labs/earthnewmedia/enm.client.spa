import encoder from "./encoder"

/**
 * This class prevents plain text password text from leaking into the react/redux stores
 * WARNING! This should not be considered secure in any way. It is simply meant
 * to prevent raw searchable strings being cached in the browser dev tools
 */
export class Password {
  constructor(value: string) {
    this.value = (): Uint8Array => encoder.strToBytes(value)
    this.equals = (x: string): boolean => value === x
    this.isSet = (): boolean => !!value
  }

  value: () => Uint8Array

  equals: (x: string) => boolean

  isSet: () => boolean

  toString(): string {
    return "********"
  }
}
