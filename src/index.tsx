import React from "react"
import ReactDOM from "react-dom"
import { Provider } from "react-redux"
import Main from "./app/Main"

import "bootstrap/dist/css/bootstrap.min.css"
import "./index.css"

import * as serviceWorker from "./serviceWorker"
import configureStore from "./store"

const store = configureStore()

const App = (): JSX.Element => (
  <Provider store={store}>
    <Main />
  </Provider>
)

ReactDOM.render(<App />, document.getElementById("root"))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
