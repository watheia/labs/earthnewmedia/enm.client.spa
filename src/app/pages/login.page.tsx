import React from "react"
import { connect } from "react-redux"

import { Redirect } from "react-router-dom"
import { Card, Col, Button, Form, Row } from "react-bootstrap"

import encoder from "../../util/encoder"
import { Password } from "../../util/password"
import { User } from "../../model"

import { authorizeUser } from "../../store/auth/actions"
import { RootState } from "../../store"
import { Dispatch } from "redux"
import { UserAuthAction } from "../../store/auth/types"

type Props = {
  isAuthenticating: boolean
  principal: User | null

  authorizeUser: (user: string, password: Password) => void
}

type State = {
  email: string
  password: Password
}

class Container extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = {
      email: "john@example.com",
      password: new Password("password2")
    }
  }

  static defaultProps = {
    isAuthenticating: false,
    principal: null
  }

  onLoginClick = (event: React.MouseEvent<HTMLButtonElement>): void => {
    event.stopPropagation()
    event.preventDefault()

    this.props.authorizeUser(this.state.email, this.state.password)
  }

  onEmailChange = (event: React.FormEvent<HTMLInputElement>): void => {
    const email = event.currentTarget.value
    if (this.state.email === email) return

    this.setState({ email })
  }

  onPasswordChange = (event: React.FormEvent<HTMLInputElement>): void => {
    if (this.state.password.equals(event.currentTarget.value)) return

    this.setState({
      password: new Password(event.currentTarget.value)
    })
  }

  render(): JSX.Element {
    const { isAuthenticating, principal } = this.props
    const { email, password } = this.state
    if (principal) {
      return <Redirect to={`/u/${principal.slug}`} />
    }
    return (
      <Col className="LoginPage">
        <Row className="justify-content-md-center">
          <Col xs={8} className="mt-4">
            <Card>
              <Card.Header as="h5">User Login</Card.Header>
              <Card.Body>
                <Form id="enm-login-form">
                  <Form.Group controlId="enm-login-email-input">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" value={email} onChange={this.onEmailChange} />
                    <Form.Text className="text-muted">We&apos;ll never share your email with anyone else.</Form.Text>
                  </Form.Group>

                  <Form.Group controlId="enm-login-password-input">
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                      type="password"
                      placeholder="Password"
                      value={encoder.bytesToStr(password.value())}
                      onChange={this.onPasswordChange}
                    />
                  </Form.Group>
                  <Form.Group controlId="enm-login-remember-check">
                    <Form.Check type="checkbox" label="RememberMe" disabled />
                  </Form.Group>
                  <Button
                    id="enm-login-submit-btn"
                    variant="primary"
                    type="submit"
                    onClick={this.onLoginClick}
                    disabled={isAuthenticating}
                  >
                    Login
                  </Button>
                </Form>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Col>
    )
  }
}

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
const mapStateToProps = (props: RootState) => {
  return {
    isAuthenticating: props.auth.isAuthenticating,
    principal: props.auth.principal
  }
}

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    authorizeUser: (user: string, password: Password): UserAuthAction => dispatch(authorizeUser(user, password))
  }
}

export const LoginPage = connect(mapStateToProps, mapDispatchToProps)(Container)
