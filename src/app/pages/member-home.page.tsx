import React from "react"
import { connect } from "react-redux"
import { Col } from "react-bootstrap"
import { RouteComponentProps, withRouter, match, Redirect } from "react-router-dom"

import { User, Library } from "../../model"
import { RootState } from "../../store"
import { Dispatch } from "redux"
import { CreateLibraryWidget, MemberLibrariesWidget } from "../components"
import {
  SpotlightEntry,
  CreateLibraryAction,
  SelectMemberInSpotlightAction,
  SelectLibraryInSpotlightAction
} from "../../store/spotlight/types"
import { createLibrary, selectMemberInSpotlight, selectLibraryInSpotlight } from "../../store/spotlight/actions"

interface RouterProps {
  userSlug: string
}

interface Props extends RouteComponentProps<RouterProps> {
  isAuthenticated: boolean
  isAuthenticating: boolean
  libraryInSpotlight: SpotlightEntry<Library>
  memberInSpotlight: SpotlightEntry<User>
  principal: User | null
  match: match<RouterProps>

  createLibrary: (entry: Partial<Library>) => void
  selectMemberInSpotlight: (slug: string | null) => void
  selectLibraryInSpotlight: (slug: string | null, entry?: Library) => void
}

type State = {
  selectedLibrary: string | null
  isCreatingLibrary: boolean
}

class Container extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)

    this.state = {
      isCreatingLibrary: false,
      selectedLibrary: null
    }
  }

  onCreateLibrarySubmit = (entry: Partial<Library>): void => {
    this.setState({ isCreatingLibrary: true })
    this.props.createLibrary(entry)
  }

  onSelectLibrary = (entry: Library): void => {
    const memberInSpotlight = this.props.memberInSpotlight.entry
    if (!memberInSpotlight) return

    this.setState({ selectedLibrary: entry.slug })
    this.props.selectLibraryInSpotlight(entry.slug, entry)
  }

  componentDidMount(): void {
    this.checkState()
  }

  componentDidUpdate(): void {
    this.checkState()
  }

  render(): JSX.Element {
    const { memberInSpotlight, libraryInSpotlight, isAuthenticated, principal } = this.props
    const { selectedLibrary, isCreatingLibrary } = this.state

    // wait for any pending load
    if (!isAuthenticated || !memberInSpotlight.entry) {
      return <Col className="MemberHomePage" />
    }

    // Redirect to selectedLibrary if any
    if (selectedLibrary) {
      return <Redirect to={`/u/${memberInSpotlight.slug}/lib/${selectedLibrary}`} />
    }

    // Redirect to newly created library once it is in the spotlight
    if (isCreatingLibrary && libraryInSpotlight.entry) {
      return <Redirect to={`/u/${memberInSpotlight.slug}/lib/${libraryInSpotlight.slug}`} />
    }

    const isSelf = principal && memberInSpotlight.entry.id === principal.id
    const libraries = memberInSpotlight.entry.libraries ? memberInSpotlight.entry.libraries : []
    return (
      <Col className="MemberHomePage">
        <MemberLibrariesWidget
          className="MemberLibrariesWidget mb-3"
          libraries={libraries}
          onSelectLibrary={this.onSelectLibrary}
        />
        {isSelf ? <CreateLibraryWidget onSubmit={this.onCreateLibrarySubmit} /> : null}
      </Col>
    )
  }

  private checkState(): void {
    const { match, libraryInSpotlight, memberInSpotlight, isAuthenticated } = this.props
    if (!isAuthenticated) return

    // Set member in spotlight if not the same
    if (match.params.userSlug !== memberInSpotlight.slug) {
      this.props.selectMemberInSpotlight(match.params.userSlug)
    }

    // Clear library if in spotlight, and nothing is being selected or created
    const { selectedLibrary, isCreatingLibrary } = this.state
    if (!libraryInSpotlight.slug || selectedLibrary || isCreatingLibrary) return
    this.props.selectLibraryInSpotlight(null)
  }
}

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
const mapStateToProps = (props: RootState) => {
  return {
    principal: props.auth.principal,
    isAuthenticated: props.auth.isAuthenticated,
    isAuthenticating: props.auth.isAuthenticating,
    memberInSpotlight: props.spotlight.member,
    libraryInSpotlight: props.spotlight.library
  }
}

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    createLibrary: (entry: Partial<Library>): CreateLibraryAction => dispatch(createLibrary(entry)),
    selectMemberInSpotlight: (slug: string | null): SelectMemberInSpotlightAction =>
      dispatch(selectMemberInSpotlight(slug)),
    selectLibraryInSpotlight: (slug: string | null, entry?: Library): SelectLibraryInSpotlightAction =>
      dispatch(selectLibraryInSpotlight(slug, entry))
  }
}

export const MemberHomePage = withRouter(connect(mapStateToProps, mapDispatchToProps)(Container))
