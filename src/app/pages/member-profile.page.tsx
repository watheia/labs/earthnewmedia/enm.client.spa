import React from "react"
import { connect } from "react-redux"
import { Card, Col } from "react-bootstrap"
import { RouteComponentProps, withRouter, Redirect, match } from "react-router-dom"

import { User } from "../../model"
import { RootState } from "../../store"
import { Dispatch } from "redux"

interface RouterProps {
  userSlug: string
}

interface Props extends RouteComponentProps<RouterProps> {
  principal: User | null
  match: match<RouterProps>
}

type State = {}

class Container extends React.Component<Props, State> {
  render(): JSX.Element {
    const { principal, match } = this.props
    if (principal && principal.slug !== match.params.userSlug) {
      return <Redirect to={`/u/${principal.slug}/profile`} />
    }

    return (
      <Col className="MemberProfilePage">
        <Card>
          <Card.Header as="h5">Member Profile</Card.Header>
        </Card>
      </Col>
    )
  }
}

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
const mapStateToProps = (props: RootState) => {
  return {
    principal: props.auth.principal
  }
}

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
const mapDispatchToProps = (dispatch: Dispatch) => {
  return {}
}

export const MemberProfilePage = withRouter(connect(mapStateToProps, mapDispatchToProps)(Container))
