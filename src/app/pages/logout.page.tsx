import React from "react"
import { connect } from "react-redux"

import { Redirect } from "react-router-dom"
import { Col } from "react-bootstrap"

import { userLogout } from "../../store/auth/actions"
import { RootState } from "../../store"
import { Dispatch } from "redux"
import { UserLogoutAction } from "../../store/auth/types"

type Props = {
  isAuthenticated: boolean

  userLogout: () => void
}

type State = {}

class Container extends React.Component<Props, State> {
  static defaultProps = {
    isAuthenticated: true
  }

  componentDidMount(): void {
    this.props.userLogout()
  }

  render(): JSX.Element {
    const { isAuthenticated } = this.props
    return isAuthenticated ? <Col className="LogoutPage" /> : <Redirect to="/" />
  }
}

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
const mapStateToProps = (props: RootState) => {
  return {
    isAuthenticated: props.auth.isAuthenticated
  }
}

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    userLogout: (): UserLogoutAction => dispatch(userLogout())
  }
}

export const LogoutPage = connect(mapStateToProps, mapDispatchToProps)(Container)
