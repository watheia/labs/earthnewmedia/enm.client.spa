import React from "react"
import { connect } from "react-redux"
import { isEqual, last } from "lodash"

import { Card, Col, Form, Row, Button } from "react-bootstrap"

import { RootState } from "../../store"
import { Dispatch } from "redux"
import { Password } from "../../util/password"
import encoder from "../../util/encoder"
import { RegisterMemberAction } from "../../store/spotlight/types"
import { registerMember } from "../../store/spotlight/actions"
import { User } from "../../model"
import { Redirect } from "react-router-dom"

type Props = {
  principal: User | null
  registerMember: (email: string, userName: string, password: Password) => void
}

type State = {
  email: string
  userName: string
  password1: Password
  password2: Password
}

class Container extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)

    this.state = {
      email: "",
      userName: "",
      password1: new Password(""),
      password2: new Password("")
    }
  }

  onEmailChange = (event: React.FormEvent<HTMLInputElement>): void => {
    const email = event.currentTarget.value
    if (this.state.email === email) return

    this.setState({ email })
  }

  onUserNameChange = (event: React.FormEvent<HTMLInputElement>): void => {
    const userName = event.currentTarget.value
    if (this.state.userName === userName) return

    this.setState({ userName })
  }

  onPasswordChange = (event: React.FormEvent<HTMLInputElement>): void => {
    const password = event.currentTarget.value
    const id = event.currentTarget.id
    if (last(id) === "1" && !this.state.password1.equals(password)) {
      this.setState({ password1: new Password(password) })
    } else if (last(id) === "2" && !this.state.password2.equals(password)) {
      this.setState({ password2: new Password(password) })
    }
  }

  onSubmitClick = (event: React.MouseEvent<HTMLButtonElement>): void => {
    event.stopPropagation()
    event.preventDefault()

    const { email, userName, password1 } = this.state
    this.props.registerMember(email, userName, password1)
  }

  render(): JSX.Element {
    const { email, userName, password1, password2 } = this.state

    // Redirect to member home once registration complete
    if (this.props.principal) {
      return <Redirect to={`/u/${this.props.principal.slug}`} />
    }

    // TODO implement proper form validation
    const isSubmitEnabled =
      !!email && !!userName && password1.isSet() && password2.isSet() && isEqual(password1.value(), password2.value())

    return (
      <Col className="RegistrationPage">
        <Row className="justify-content-md-center">
          <Col xs={8} className="mt-4">
            <Card id="enm-registration-card">
              <Card.Header as="h5">Member Registration</Card.Header>
              <Card.Body>
                <Form>
                  <Form.Group controlId="enm-registration-email-input">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" value={email} onChange={this.onEmailChange} />
                    <Form.Text className="text-muted">We&apos;ll never share your email with anyone else.</Form.Text>
                  </Form.Group>
                  <Form.Group controlId="enm-registration-username-input">
                    <Form.Label>User Name</Form.Label>
                    <Form.Control
                      type="input"
                      placeholder="Desired user name..."
                      value={userName}
                      onChange={this.onUserNameChange}
                    />
                  </Form.Group>
                  <Form.Group controlId="enm-registration-password-input-1">
                    <Form.Label>Password 1</Form.Label>
                    <Form.Control
                      type="password"
                      placeholder="Select a good password..."
                      value={encoder.bytesToStr(password1.value())}
                      onChange={this.onPasswordChange}
                    />
                  </Form.Group>
                  <Form.Group controlId="enm-registration-password-input-2">
                    <Form.Label>Password 2</Form.Label>
                    <Form.Control
                      type="password"
                      placeholder="Now input it again here"
                      value={encoder.bytesToStr(password2.value())}
                      onChange={this.onPasswordChange}
                    />
                  </Form.Group>
                  <Button
                    id="enm-registration-submit-btn"
                    variant="primary"
                    type="submit"
                    onClick={this.onSubmitClick}
                    disabled={!isSubmitEnabled}
                  >
                    Submit
                  </Button>
                </Form>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Col>
    )
  }
}

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
const mapStateToProps = (props: RootState) => {
  return {
    isAuthenticated: props.auth.isAuthenticated,
    principal: props.auth.principal
  }
}

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    registerMember: (email: string, userName: string, password: Password): RegisterMemberAction =>
      dispatch(registerMember(email, userName, password))
  }
}

export const RegistrationPage = connect(mapStateToProps, mapDispatchToProps)(Container)
