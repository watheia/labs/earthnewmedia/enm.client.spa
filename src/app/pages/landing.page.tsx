import React from "react"
import { connect } from "react-redux"
import { Col, Row } from "react-bootstrap"
import { withRouter } from "react-router-dom"
import { Dispatch } from "redux"

import { RootState } from "../../store"

type Props = {}

type State = {}

const logo = "/logo.png"

class Container extends React.Component<Props, State> {
  static defaultProps = {
    isAuthenticated: false
  }

  render(): JSX.Element {
    return (
      <Col className="LandingPage">
        <Row className="justify-content-md-center">
          <Col md="auto">
            <img id="enm-hero-img" className="img-responsive" src={logo} alt="logo" />
          </Col>
        </Row>
      </Col>
    )
  }
}

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
const mapStateToProps = (props: RootState) => {
  return {}
}

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
const mapDispatchToProps = (dispatch: Dispatch) => {
  return {}
}

export const LandingPage = withRouter(connect(mapStateToProps, mapDispatchToProps)(Container))
