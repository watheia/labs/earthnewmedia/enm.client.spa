import React from "react"
import { connect } from "react-redux"
import { find } from "lodash"
import { Card, Col, ListGroup, ButtonGroup, Button, ButtonToolbar, InputGroup, Form } from "react-bootstrap"
import { RouteComponentProps, withRouter, match, Redirect } from "react-router-dom"

import { User, Library, AccessLevel } from "../../model"
import { RootState } from "../../store"
import { Dispatch } from "redux"
import {
  SpotlightEntry,
  SelectMemberInSpotlightAction,
  SelectLibraryInSpotlightAction,
  DeleteLibraryAction
} from "../../store/spotlight/types"
import { selectMemberInSpotlight, selectLibraryInSpotlight, deleteLibrary } from "../../store/spotlight/actions"

import "./member-library.page.css"

interface RouterProps {
  librarySlug: string
  userSlug: string
}

interface Props extends RouteComponentProps<RouterProps> {
  isAuthenticated: boolean
  match: match<RouterProps>
  libraryInSpotlight: SpotlightEntry<Library>
  memberInSpotlight: SpotlightEntry<User>

  deleteLibrary: (entry: Library) => void
  selectMemberInSpotlight: (slug: string | null, entry?: User) => void
  selectLibraryInSpotlight: (slug: string | null, entry?: Library) => void
}

type State = {
  isDeleting: boolean
  access: AccessLevel
}

class Container extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)

    this.state = {
      isDeleting: false,
      access: AccessLevel.Private
    }
  }

  onAccessChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
    const { libraryInSpotlight } = this.props
    if (!libraryInSpotlight.entry) return

    libraryInSpotlight.entry.access = event.currentTarget.checked ? AccessLevel.Public : AccessLevel.Private
    this.setState({ access: libraryInSpotlight.entry.access })
  }

  onDeleteClick = (): void => {
    const { libraryInSpotlight } = this.props
    if (!libraryInSpotlight.entry) return

    this.setState({ isDeleting: true })
    this.props.deleteLibrary(libraryInSpotlight.entry)
  }

  componentDidMount(): void {
    this.checkState()
  }

  componentDidUpdate(): void {
    this.checkState()
  }

  render(): JSX.Element {
    const { libraryInSpotlight, memberInSpotlight, isAuthenticated } = this.props

    // Redirect to member home after library is deleted
    if (this.state.isDeleting && !libraryInSpotlight.entry) {
      return <Redirect to={`/u/${memberInSpotlight.slug}`} />
    }

    // wait for any pending load
    if (!isAuthenticated || !memberInSpotlight.entry || !libraryInSpotlight.entry) {
      return <Col className="MemberLibraryPage" />
    }

    const lib = libraryInSpotlight.entry
    return (
      <Col className="MemberLibraryPage h-100">
        <Card id="enm-library-card" className="h-100">
          <Card.Header>
            <Card.Title id="enm-library-name" className="d-inline-block">
              {lib.name}
            </Card.Title>
            <Card.Subtitle id="enm-library-type" className="ml-1 mb-2 text-muted d-inline-block">
              ({lib.libraryType})
            </Card.Subtitle>
            <ButtonToolbar aria-label="Library Tools" className="float-right align-items-center">
              <InputGroup>
                <Form.Check
                  id="enm-library-access-check"
                  type="switch"
                  inline
                  checked={this.state.access === AccessLevel.Public}
                  label={this.state.access}
                  onChange={this.onAccessChange}
                />
              </InputGroup>
              <ButtonGroup aria-label="Library Tools - Button Bar" className="float-right">
                <Button id="enm-library-edit-btn" variant="secondary" disabled>
                  Edit
                </Button>
                <Button id="enm-library-delete-btn" variant="secondary" onClick={this.onDeleteClick}>
                  Delete
                </Button>
              </ButtonGroup>
            </ButtonToolbar>
          </Card.Header>
          <Card.Body>
            <Card.Text id="enm-library-description">{lib.description}</Card.Text>
            <Card.Text id="enm-library-created-date">
              Created: <span>Today</span>
            </Card.Text>
            <Card.Text id="enm-library-last-modified-date">
              Last Modified: <span>Today</span>
            </Card.Text>
            <ListGroup id="enm-library-items-list" variant="flush">
              <ListGroup.Item>Cras justo odio</ListGroup.Item>
              <ListGroup.Item>Dapibus ac facilisis in</ListGroup.Item>
              <ListGroup.Item>Morbi leo risus</ListGroup.Item>
              <ListGroup.Item>Porta ac consectetur ac</ListGroup.Item>
            </ListGroup>
          </Card.Body>
        </Card>
      </Col>
    )
  }

  private checkState(): void {
    const { match, libraryInSpotlight, memberInSpotlight, isAuthenticated } = this.props
    if (!isAuthenticated) return

    if (match.params.userSlug !== memberInSpotlight.slug) {
      this.props.selectMemberInSpotlight(match.params.userSlug)
    }

    if (memberInSpotlight.entry && !this.state.isDeleting && match.params.librarySlug !== libraryInSpotlight.slug) {
      const libraries = memberInSpotlight.entry.libraries
      if (libraries) {
        const slug = match.params.librarySlug
        const entry = find(libraries, { slug })
        this.props.selectLibraryInSpotlight(slug, entry)
      }
    }

    if (this.props.libraryInSpotlight.entry && this.props.libraryInSpotlight.entry.access !== this.state.access) {
      this.setState({ access: this.props.libraryInSpotlight.entry.access })
    }
  }
}

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
const mapStateToProps = (props: RootState) => {
  return {
    principal: props.auth.principal,
    isAuthenticated: props.auth.isAuthenticated,
    memberInSpotlight: props.spotlight.member,
    libraryInSpotlight: props.spotlight.library
  }
}

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    deleteLibrary: (entry: Library): DeleteLibraryAction => dispatch(deleteLibrary(entry)),
    selectMemberInSpotlight: (slug: string | null, entry?: User): SelectMemberInSpotlightAction =>
      dispatch(selectMemberInSpotlight(slug, entry)),
    selectLibraryInSpotlight: (slug: string | null, entry?: Library): SelectLibraryInSpotlightAction =>
      dispatch(selectLibraryInSpotlight(slug, entry))
  }
}

export const MemberLibraryPage = withRouter(connect(mapStateToProps, mapDispatchToProps)(Container))
