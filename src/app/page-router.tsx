import React from "react"
import { Route, Switch } from "react-router-dom"
import { PrivateRoute } from "./components"
import { LandingPage } from "./pages/landing.page"
import { LoginPage } from "./pages/login.page"
import { RegistrationPage } from "./pages/registration.page"
import { LogoutPage } from "./pages/logout.page"
import { MemberHomePage } from "./pages/member-home.page"
import { MemberAccountPage } from "./pages/member-account.page"
import { MemberProfilePage } from "./pages/member-profile.page"
import { MemberLibraryPage } from "./pages/member-library.page"

type Props = {
  isAuthenticated: boolean
  isAuthenticating: boolean
}

export const PageRouter = (props: Props): JSX.Element => {
  const { isAuthenticated, isAuthenticating } = props
  return (
    <Switch>
      <Route path="/" exact>
        <LandingPage />
      </Route>
      <Route path="/login" exact>
        <LoginPage />
      </Route>
      <Route path="/register" exact>
        <RegistrationPage />
      </Route>
      <Route path="/logout" exact>
        <LogoutPage />
      </Route>
      <PrivateRoute
        path="/u/:userSlug"
        isAuthenticated={isAuthenticated}
        isAuthenticating={isAuthenticating}
        component={MemberHomePage}
        exact
      />
      <PrivateRoute
        path="/u/:userSlug/account"
        isAuthenticated={isAuthenticated}
        isAuthenticating={isAuthenticating}
        component={MemberAccountPage}
        exact
      />
      <PrivateRoute
        path="/u/:userSlug/profile"
        isAuthenticated={isAuthenticated}
        isAuthenticating={isAuthenticating}
        component={MemberProfilePage}
        exact
      />
      <PrivateRoute
        path="/u/:userSlug/lib/:librarySlug"
        isAuthenticated={isAuthenticated}
        isAuthenticating={isAuthenticating}
        component={MemberLibraryPage}
        exact
      />
    </Switch>
  )
}
