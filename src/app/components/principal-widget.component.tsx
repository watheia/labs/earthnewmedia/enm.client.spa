import React from "react"
import { User } from "../../model"
import { Card, ListGroup, ListGroupItem } from "react-bootstrap"

type Props = {
  className: string
  principal: User
}

type State = {}

export class PrincipalWidget extends React.Component<Props, State> {
  static defaultProps = {
    className: "PrincipalUserCard"
  }

  render(): JSX.Element {
    const { principal } = this.props
    return (
      <Card id="enm-principal-card">
        <Card.Img id="enm-principal-avatar" variant="top" src="img/placeholder-150x150.jpg" />
        <Card.Body>
          <Card.Title id="enm-principal-username">{principal.userName}</Card.Title>
        </Card.Body>
        <ListGroup className="list-group-flush">
          <ListGroupItem>
            <Card.Link id="enm-principal-nav-account" href={`#/u/${principal.slug}/account`}>
              Account
            </Card.Link>
          </ListGroupItem>
          <ListGroupItem>
            <Card.Link id="enm-principal-nav-profile" href={`#/u/${principal.slug}/profile`}>
              Profile
            </Card.Link>
          </ListGroupItem>
          <ListGroupItem>
            <Card.Link id="enm-principal-nav-logout" href="#/logout">
              Logout
            </Card.Link>
          </ListGroupItem>
        </ListGroup>
        <Card.Body></Card.Body>
      </Card>
    )
  }
}
