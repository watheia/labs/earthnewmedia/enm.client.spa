import React from "react"
import { Card, ListGroup } from "react-bootstrap"
import { Library } from "../../model"
import { isEmpty, map } from "lodash"

type Props = {
  className: string
  libraries: Library[]
  onSelectLibrary: (entry: Library) => void
}

type State = {}

export class MemberLibrariesWidget extends React.Component<Props, State> {
  static defaultProps = {
    className: "MemberLibrariesWidget"
  }

  onSelectLibrary = (entry: Library): void => {
    this.props.onSelectLibrary(entry)
  }

  render(): JSX.Element {
    const { libraries } = this.props
    return (
      <Card id="enm-member-libraries-card" className={this.props.className}>
        <Card.Header>Member Libraries</Card.Header>
        <Card.Body>
          {isEmpty(libraries) ? (
            <p>
              You have not created any libraries yet. Use the &quot;Create New Library&quot; tool above to create your
              first Library
            </p>
          ) : null}
          <ListGroup id="enm-member-libraries-list" variant="flush">
            {map(libraries, lib => (
              <ListGroup.Item key={lib.id} data-slug={lib.slug} action onClick={(): void => this.onSelectLibrary(lib)}>
                {lib.name}
              </ListGroup.Item>
            ))}
          </ListGroup>
        </Card.Body>
      </Card>
    )
  }
}
