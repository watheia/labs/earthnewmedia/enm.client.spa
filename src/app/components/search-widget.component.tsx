import React from "react"
import { Form, FormControl, Button } from "react-bootstrap"

type Props = {
  className: string
}

type State = {}

export class SearchWidget extends React.Component<Props, State> {
  static defaultProps = {
    className: "SearchWidget"
  }

  render(): JSX.Element {
    return (
      <Form inline id="enm-search-form" className={this.props.className}>
        <FormControl id="enm-search-text-input" type="text" placeholder="Search" className="mr-sm-2" />
        <Button id="enm-search-submit">Search</Button>
      </Form>
    )
  }
}
