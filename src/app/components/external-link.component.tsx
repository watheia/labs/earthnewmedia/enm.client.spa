import React, { CSSProperties } from "react"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faExternalLinkAlt } from "@fortawesome/free-solid-svg-icons"

type Props = {
  uri: string
  text?: string
}

const STYLE: CSSProperties = { verticalAlign: "middle" }

export const ExternalLink = (props: Props): JSX.Element => {
  const linkText = props.text ? props.text : props.uri
  return (
    <a href={props.uri} target="__blank" style={STYLE}>
      {linkText}
      <FontAwesomeIcon className="ml-2 fa-xs" icon={faExternalLinkAlt} />
    </a>
  )
}
