import React from "react"
import { Card, Form, Row, Col, Button } from "react-bootstrap"
import { LibraryType, AccessLevel, Library } from "../../model"

type Props = {
  className: string
  onSubmit: (entry: Partial<Library>) => void
}

type State = {
  access: AccessLevel
  libraryType: LibraryType | null
  name: string
  description: string
}

export class CreateLibraryWidget extends React.Component<Props, State> {
  static defaultProps = {
    className: "CreateLibraryWidget"
  }

  constructor(props: Props) {
    super(props)
    this.state = {
      access: AccessLevel.Private,
      name: "",
      description: "",
      libraryType: null
    }
  }

  onLibraryTypeChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
    const libraryType = LibraryType[event.currentTarget.value as keyof typeof LibraryType]
    if (libraryType === this.state.libraryType) return

    this.setState({ libraryType })
  }

  onNameChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
    const name: string = event.currentTarget.value
    if (name === this.state.name) return

    this.setState({ name })
  }

  onDescriptionChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
    const description: string = event.currentTarget.value
    if (description === this.state.description) return

    this.setState({ description })
  }

  onAccessChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
    const access: AccessLevel = event.currentTarget.checked ? AccessLevel.Public : AccessLevel.Private
    this.setState({ access })
  }

  onSubmit = (event: React.MouseEvent<HTMLButtonElement>): void => {
    event.stopPropagation()
    event.preventDefault()

    const { access, libraryType, name, description } = this.state

    // TODO this should be caught by form validation error
    if (libraryType == null) throw new Error("Invalid LibraryType selected")
    if (!name) throw new Error("Invalid library name selected")

    this.props.onSubmit({ access, libraryType, name, description })
  }

  render(): JSX.Element {
    const { access, name, description, libraryType } = this.state
    const isSubmitEnabled = libraryType !== null && name
    return (
      <Card id="enm-create-library-card" className={this.props.className}>
        <Card.Header>Create New Library</Card.Header>
        <Card.Body>
          <Form>
            <Row>
              <Col>
                <Form.Group controlId="enm-create-library-type-select">
                  <Form.Control
                    as="select"
                    placeholder="Library Type"
                    value={String(libraryType)}
                    onChange={this.onLibraryTypeChange}
                  >
                    <option value="null" disabled>
                      Select Type
                    </option>
                    <option value="Discussion">Discussion</option>
                    <option value="Album">Album</option>
                    <option value="Gallery">Gallery</option>
                  </Form.Control>
                </Form.Group>
                <Form.Group controlId="enm-create-library-name-input">
                  <Form.Control placeholder="Library Name" value={name} onChange={this.onNameChange} />
                  <Form.Text className="text-muted">A short, memorable, and unique name for the library.</Form.Text>
                </Form.Group>
              </Col>
              <Col>
                <Form.Group controlId="enm-create-library-desciption-input">
                  <Form.Control
                    as="textarea"
                    rows="3"
                    placeholder="Optional description of this library..."
                    onChange={this.onDescriptionChange}
                    value={description}
                  />
                </Form.Group>
                <Form.Row className="align-items-center">
                  <Col />
                  <Form.Check
                    id="enm-create-library-access-check"
                    className="col col-sm-auto"
                    type="switch"
                    inline
                    checked={access === AccessLevel.Private ? false : true}
                    label={access}
                    onChange={this.onAccessChange}
                  />
                  <Button
                    className="col col-sm-auto"
                    id="enm-create-library-submit-btn"
                    variant="primary"
                    type="submit"
                    disabled={!isSubmitEnabled}
                    onClick={this.onSubmit}
                  >
                    Create
                  </Button>
                </Form.Row>
              </Col>
            </Row>
          </Form>
        </Card.Body>
      </Card>
    )
  }
}
