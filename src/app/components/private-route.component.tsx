import React, { ComponentClass } from "react"
import { Redirect, Route } from "react-router-dom"

type Props = {
  path: string
  component: ComponentClass<any, any>
  isAuthenticated: boolean
  isAuthenticating: boolean
  exact: boolean
}

export const PrivateRoute = (props: Props): JSX.Element => {
  const { isAuthenticated, isAuthenticating, component, ...rest } = props
  const routeComponent = (props: any): JSX.Element =>
    isAuthenticating || isAuthenticated ? React.createElement(component, props) : <Redirect to="/login" />
  return <Route {...rest} render={routeComponent} />
}
