import React, { Component } from "react"

import { connect } from "react-redux"
import { Container, Nav, Navbar, Row, Col } from "react-bootstrap"
import { HashRouter as Router } from "react-router-dom"
import { Dispatch } from "redux"

import { RootState } from "../store"
import { restoreSession } from "../store/auth/actions"
import { PrincipalWidget } from "./components"
import { User } from "../model"
import { PageRouter } from "./page-router"

import "./Main.css"
import { SearchWidget } from "./components/search-widget.component"

type Props = {
  principal: User | null
  isAuthenticated: boolean
  isAuthenticating: boolean
  restoreSession: (token: string) => void
}

type State = {}

class MainComponent extends Component<Props, State> {
  componentDidMount(): void {
    const token = sessionStorage.getItem("token")
    this.props.restoreSession(token ? token : "")
  }

  static defaultProps = {
    principal: null,
    isAuthenticated: false,
    isAuthenticating: true
  }

  render(): JSX.Element {
    const { principal, isAuthenticating, isAuthenticated } = this.props
    return (
      <Router>
        <Container className="h-100 bg-dark">
          <Navbar bg="dark" variant="dark">
            {principal ? (
              <Navbar.Brand id="enm-nav-home" href={`/#/u/${principal.slug}`}>
                earthnew.media
              </Navbar.Brand>
            ) : (
              <Navbar.Brand id="enm-nav-home" href="/#">
                earthnew.media
              </Navbar.Brand>
            )}
            <Nav className="mr-auto">
              {isAuthenticated ? null : (
                <Nav.Item id="enm-nav-login">
                  <Nav.Link disabled={isAuthenticating} href="#login">
                    Login
                  </Nav.Link>
                </Nav.Item>
              )}
              {isAuthenticated ? null : (
                <Nav.Item id="enm-nav-register">
                  <Nav.Link href="#register">Register</Nav.Link>
                </Nav.Item>
              )}
            </Nav>
            <SearchWidget />
          </Navbar>
          <Row className="h-100">
            <Col md="auto">{principal ? <PrincipalWidget principal={principal} /> : null}</Col>
            <PageRouter isAuthenticated={isAuthenticated} isAuthenticating={isAuthenticating} />
          </Row>
        </Container>
      </Router>
    )
  }
}

const mapStateToProps = (props: RootState) => {
  return {
    ...props.auth
  }
}

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    restoreSession: (token: string) => dispatch(restoreSession(token))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MainComponent)
