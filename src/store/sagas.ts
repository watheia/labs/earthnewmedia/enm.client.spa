/* eslint-disable @typescript-eslint/explicit-function-return-type */
import { put, takeLatest, select } from "redux-saga/effects"
import { find } from "lodash"

import { USER_AUTH, SESSION_RESTORE, USER_LOGOUT, UserAuthAction, SessionRestoreAction } from "./auth/types"
import { authorizeUserSuccess, authorizeUserFailed, restoreSessionSuccess, restoreSessionFailed } from "./auth/actions"

import {
  CREATE_LIBRARY,
  SELECT_MEMBER_IN_SPOTLIGHT,
  CreateLibraryAction,
  SelectMemberInSpotlightAction,
  SelectLibraryInSpotlightAction,
  SELECT_LIBRARY_IN_SPOTLIGHT,
  DeleteLibraryAction,
  DELETE_LIBRARY,
  REGISTER_MEMBER,
  RegisterMemberAction
} from "./spotlight/types"
import {
  selectMemberInSpotlightSuccess,
  createLibrarySuccess,
  selectMemberInSpotlight,
  selectLibraryInSpotlight,
  selectLibraryInSpotlightSuccess,
  deleteLibrarySuccess,
  registerMemberSuccess
} from "./spotlight/actions"

import { AuthApi } from "../api/auth.api"
import { UserApi } from "../api/user.api"
import { systemMessage } from "./sys/actions"
import { SystemMessage } from "../model/system-message.model"
import { LibraryApi } from "../api/library.api"
import { RootState } from "."
import { User, Library } from "../model"

const API_GATEWAY = "http://localhost:3000"

const authApi = new AuthApi(API_GATEWAY)
const userApi = new UserApi(API_GATEWAY)
const libraryApi = new LibraryApi(API_GATEWAY)

const getPrincipal = (state: RootState): User | null => state.auth.principal
const getToken = (state: RootState): string | null => (state.auth.token ? state.auth.token : "")
const getMemberInSpotlight = (state: RootState): User | null => state.spotlight.member.entry
const getLibraryInSpotlight = (state: RootState): Library | null => state.spotlight.library.entry

function storeToken(token: string) {
  sessionStorage.setItem("token", token)
}

function* authorizeUserSaga(action: UserAuthAction) {
  try {
    const { token } = yield authApi.login(action.payload.email, action.payload.password)
    try {
      userApi.token = token
      const entry = yield userApi.me()
      if (entry) {
        storeToken(token)
        yield put(selectMemberInSpotlight(entry.slug, { ...entry }))
        yield put(authorizeUserSuccess(token, entry))
      } else {
        put(authorizeUserFailed(new Error("Unkown error while loading Principal.")))
      }
    } catch (e) {
      console.error(e)
      yield put(authorizeUserFailed(e))
    }
  } catch (e) {
    console.error(e)
    yield put(authorizeUserFailed(new Error("Invalid username and/or password.")))
  }
}

function* restoreSessionSaga(action: SessionRestoreAction) {
  if (!action.payload.token) {
    yield put(restoreSessionFailed())
  } else {
    try {
      userApi.token = action.payload.token
      const entry = yield userApi.me()
      if (entry) {
        storeToken(action.payload.token)
        yield put(selectMemberInSpotlight(entry.slug, { ...entry }))
        yield put(restoreSessionSuccess(action.payload.token, entry))
      } else {
        yield put(restoreSessionFailed())
      }
    } catch (e) {
      console.error(e)
      storeToken("")
      yield put(restoreSessionFailed())
    }
  }
}

function* userLogoutSaga() {
  try {
    storeToken("")
    yield put(selectLibraryInSpotlight(null))
    yield put(selectMemberInSpotlight(null))
  } catch (e) {
    console.error(e)
  }
}

function* selectMemberInSpotlightSaga(action: SelectMemberInSpotlightAction) {
  if (!action.payload.userSlug) return true

  // load entry if not already present
  if (!action.payload.entry) {
    try {
      userApi.token = yield select(getToken)
      const entry = yield userApi.findBySlug(action.payload.userSlug)
      yield put(selectMemberInSpotlightSuccess(entry))
    } catch (e) {
      console.error(e)
      yield put(systemMessage(SystemMessage.error(e.message)))
    }
  }
}

function* selectLibraryInSpotlightSaga(action: SelectLibraryInSpotlightAction) {
  if (!action.payload.librarySlug) return true

  // load entry if not already present
  if (!action.payload.entry) {
    try {
      const memberInSpotlight: User = yield select(getMemberInSpotlight)
      if (!memberInSpotlight) {
        throw new Error("Member in the spotlight is not in focus")
      }

      const libraries = memberInSpotlight.libraries ? memberInSpotlight.libraries : []
      const entry = find(libraries, { slug: action.payload.librarySlug })
      if (entry) {
        yield put(selectLibraryInSpotlightSuccess(entry))
      } else {
        yield put(selectLibraryInSpotlight(null))
        yield put(systemMessage(SystemMessage.error(`Member does not have the Library: ${action.payload.librarySlug}`)))
      }
    } catch (e) {
      console.error(e)
      yield put(systemMessage(SystemMessage.error(e.message)))
    }
  }
}

function* createLibrarySaga(action: CreateLibraryAction) {
  try {
    libraryApi.token = yield select(getToken)

    const principal = yield select(getPrincipal)
    const entry = yield libraryApi.createLibrary(principal.id, action.payload.entry)
    yield put(createLibrarySuccess(entry))
    yield put(selectLibraryInSpotlight(entry.slug, entry))
  } catch (e) {
    console.error(e)
    yield put(systemMessage(SystemMessage.error(e.message)))
  }
}

function* deleteLibrarySaga(action: DeleteLibraryAction) {
  try {
    libraryApi.token = yield select(getToken)

    const principal = yield select(getPrincipal)
    const entry = yield libraryApi.deleteLibrary(principal.id, action.payload.entry.id)
    if (entry.count !== 1) {
      throw new Error("An unknown error occurred while attempting to delete a Library.")
    }
    yield put(deleteLibrarySuccess(entry.count))
    const libraryInSpotlight = yield select(getLibraryInSpotlight)
    if (libraryInSpotlight) {
      yield put(selectLibraryInSpotlight(null))
    }
  } catch (e) {
    console.error(e)
    yield put(systemMessage(SystemMessage.error(e.message)))
  }
}

function* registerMemberSaga(action: RegisterMemberAction) {
  try {
    const { email, userName, password } = action.payload
    const { token } = yield userApi.create(email, userName, password)
    userApi.token = token
    const entry = yield userApi.me()
    if (entry) {
      storeToken(token)
      yield put(selectMemberInSpotlight(entry.slug, { ...entry }))
      yield put(registerMemberSuccess(entry))
      yield put(restoreSessionSuccess(token, entry))
    } else {
      throw new Error("Member registration failed with an unknown error.")
    }
  } catch (e) {
    console.error(e)
    yield put(systemMessage(SystemMessage.error(e.message)))
  }
}

function* rootSaga() {
  yield takeLatest(USER_AUTH, authorizeUserSaga)
  yield takeLatest(SESSION_RESTORE, restoreSessionSaga)
  yield takeLatest(USER_LOGOUT, userLogoutSaga)
  yield takeLatest(SELECT_MEMBER_IN_SPOTLIGHT, selectMemberInSpotlightSaga)
  yield takeLatest(SELECT_LIBRARY_IN_SPOTLIGHT, selectLibraryInSpotlightSaga)
  yield takeLatest(CREATE_LIBRARY, createLibrarySaga)
  yield takeLatest(DELETE_LIBRARY, deleteLibrarySaga)
  yield takeLatest(REGISTER_MEMBER, registerMemberSaga)
}

export default rootSaga
