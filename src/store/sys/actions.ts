import { HANDLE_SYS_MESSAGE, SystemActionTypes } from "./types"
import { SystemMessage } from "../../model/system-message.model"

export function systemMessage(message: SystemMessage): SystemActionTypes {
  return {
    type: HANDLE_SYS_MESSAGE,
    payload: { message }
  }
}
