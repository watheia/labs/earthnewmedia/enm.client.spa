import { concat } from "lodash"

import { SystemState, SystemActionTypes, HANDLE_SYS_MESSAGE } from "./types"

const initialState: SystemState = {
  messages: []
}

export function sysReducer(state = initialState, action: SystemActionTypes): SystemState {
  switch (action.type) {
    case HANDLE_SYS_MESSAGE: {
      return {
        messages: concat(state.messages, action.payload.message)
      }
    }
    default:
      return state
  }
}
