import { SystemMessage } from "../../model/system-message.model"

export const HANDLE_SYS_MESSAGE = "HANDLE_SYS_MESSAGE"

export interface SystemState {
  messages: SystemMessage[]
}

interface HandleSystemMessageAction {
  type: typeof HANDLE_SYS_MESSAGE
  payload: { message: SystemMessage }
}

export type SystemActionTypes = HandleSystemMessageAction
