import { Password } from "../../util/password"
import { User } from "../../model"

export const USER_AUTH = "USER_AUTH"
export const USER_AUTH_S = "USER_AUTH_S"
export const USER_AUTH_F = "USER_AUTH_F"

export const USER_LOGOUT = "USER_LOGOUT"

export const SESSION_RESTORE = "SESSION_RESTORE"
export const SESSION_RESTORE_F = "SESSION_RESTORE_F"
export const SESSION_RESTORE_S = "SESSION_RESTORE_S"

export interface UserState {
  isAuthenticated: boolean
  isAuthenticating: boolean
  principal: User | null
  token: string | null
}

export interface UserAuthAction {
  type: typeof USER_AUTH
  payload: { email: string; password: Password }
}

export interface UserAuthSuccessAction {
  type: typeof USER_AUTH_S
  payload: { token: string; principal: User }
}

export interface UserAuthFailAction {
  type: typeof USER_AUTH_F
  payload: { error: Error }
}

export interface SessionRestoreAction {
  type: typeof SESSION_RESTORE
  payload: { token: string }
}

export interface SessionRestoreFailAction {
  type: typeof SESSION_RESTORE_F
}

export interface SessionRestoreSuccessAction {
  type: typeof SESSION_RESTORE_S
  payload: { token: string; principal: User }
}

export interface UserLogoutAction {
  type: typeof USER_LOGOUT
}

export type UserActionTypes =
  | SessionRestoreAction
  | SessionRestoreSuccessAction
  | SessionRestoreFailAction
  | UserAuthAction
  | UserAuthSuccessAction
  | UserAuthFailAction
  | UserLogoutAction
