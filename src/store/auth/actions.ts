import { Password } from "../../util/password"
import { User } from "../../model"

import {
  USER_AUTH,
  USER_AUTH_S,
  USER_AUTH_F,
  USER_LOGOUT,
  SESSION_RESTORE,
  SESSION_RESTORE_S,
  SESSION_RESTORE_F,
  UserLogoutAction,
  UserAuthFailAction,
  UserAuthSuccessAction,
  SessionRestoreAction,
  UserAuthAction,
  SessionRestoreSuccessAction,
  SessionRestoreFailAction
} from "./types"

export function authorizeUser(email: string, password: Password): UserAuthAction {
  return {
    type: USER_AUTH,
    payload: { email, password }
  }
}

export function authorizeUserSuccess(token: string, principal: User): UserAuthSuccessAction {
  return {
    type: USER_AUTH_S,
    payload: { principal, token }
  }
}

export function authorizeUserFailed(error: Error): UserAuthFailAction {
  return {
    type: USER_AUTH_F,
    payload: { error }
  }
}

export function restoreSession(token: string): SessionRestoreAction {
  return {
    type: SESSION_RESTORE,
    payload: { token }
  }
}

export function restoreSessionSuccess(token: string, principal: User): SessionRestoreSuccessAction {
  return {
    type: SESSION_RESTORE_S,
    payload: { principal, token }
  }
}

export function restoreSessionFailed(): SessionRestoreFailAction {
  return {
    type: SESSION_RESTORE_F
  }
}

export function userLogout(): UserLogoutAction {
  return { type: USER_LOGOUT }
}
