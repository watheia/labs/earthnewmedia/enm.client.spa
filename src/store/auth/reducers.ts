import {
  UserState,
  UserActionTypes,
  USER_AUTH,
  USER_AUTH_S,
  USER_AUTH_F,
  USER_LOGOUT,
  SESSION_RESTORE,
  SESSION_RESTORE_F,
  SESSION_RESTORE_S
} from "./types"

const initialState: UserState = {
  isAuthenticated: false,
  isAuthenticating: true,
  principal: null,
  token: null
}

export function authReducer(state: UserState = initialState, action: UserActionTypes): UserState {
  switch (action.type) {
    case USER_AUTH:
      return {
        ...state,
        isAuthenticating: true
      }
    case USER_AUTH_S:
      return {
        ...state,
        isAuthenticated: true,
        isAuthenticating: false,
        principal: action.payload.principal,
        token: action.payload.token
      }
    case USER_AUTH_F:
      return {
        ...state,
        principal: null,
        token: null,
        isAuthenticating: false
      }
    case SESSION_RESTORE:
      return {
        ...state,
        isAuthenticating: true
      }
    case SESSION_RESTORE_F:
      return {
        ...state,
        isAuthenticating: false,
        principal: null,
        token: null
      }
    case SESSION_RESTORE_S:
      return {
        ...state,
        isAuthenticated: true,
        isAuthenticating: false,
        principal: action.payload.principal,
        token: action.payload.token
      }
    case USER_LOGOUT:
      return {
        isAuthenticated: false,
        isAuthenticating: false,
        principal: null,
        token: null
      }
    default:
      return state
  }
}
