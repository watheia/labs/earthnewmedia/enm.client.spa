import { User, Library } from "../../model"
import { Password } from "../../util/password"

export const SELECT_MEMBER_IN_SPOTLIGHT = "SELECT_MEMBER_IN_SPOTLIGHT"
export const SELECT_MEMBER_IN_SPOTLIGHT_S = "SELECT_MEMBER_IN_SPOTLIGHT_S"

export const SELECT_LIBRARY_IN_SPOTLIGHT = "SELECT_LIBRARY_IN_SPOTLIGHT"
export const SELECT_LIBRARY_IN_SPOTLIGHT_S = "SELECT_LIBRARY_IN_SPOTLIGHT_S"

export const CREATE_LIBRARY = "CREATE_LIBRARY"
export const CREATE_LIBRARY_S = "CREATE_LIBRARY_S"

export const DELETE_LIBRARY = "DELETE_LIBRARY"
export const DELETE_LIBRARY_S = "DELETE_LIBRARY_S"

export const REGISTER_MEMBER = "REGISTER_MEMBER"
export const REGISTER_MEMBER_S = "REGISTER_MEMBER_S"

export class SpotlightEntry<T> {
  slug: string | null

  /**
   * always null if slug is null
   */
  entry: T | null

  static with<T>(slug: string | null = null, entry: T | null = null): SpotlightEntry<T> {
    return new SpotlightEntry(slug, entry)
  }

  private constructor(slug: string | null, entry: T | null) {
    this.slug = slug
    this.entry = slug ? entry : null
  }
}

export interface SpotlightState {
  member: SpotlightEntry<User>
  library: SpotlightEntry<Library>
}

export interface SelectMemberInSpotlightAction {
  type: typeof SELECT_MEMBER_IN_SPOTLIGHT
  payload: { userSlug: string | null; entry?: User }
}

export interface SelectMemberInSpotlightSuccessAction {
  type: typeof SELECT_MEMBER_IN_SPOTLIGHT_S
  payload: { entry: User }
}

export interface SelectLibraryInSpotlightAction {
  type: typeof SELECT_LIBRARY_IN_SPOTLIGHT
  payload: { librarySlug: string | null; entry?: Library }
}

export interface SelectLibraryInSpotlightSuccessAction {
  type: typeof SELECT_LIBRARY_IN_SPOTLIGHT_S
  payload: { entry: Library }
}

export interface CreateLibraryAction {
  type: typeof CREATE_LIBRARY
  payload: { entry: Partial<Library> }
}

export interface CreateLibrarySuccessAction {
  type: typeof CREATE_LIBRARY_S
  payload: { entry: Library }
}

export interface DeleteLibraryAction {
  type: typeof DELETE_LIBRARY
  payload: { entry: Library }
}

export interface DeleteLibrarySuccessAction {
  type: typeof DELETE_LIBRARY_S
  payload: { count: number }
}

export interface RegisterMemberAction {
  type: typeof REGISTER_MEMBER
  payload: { email: string; userName: string; password: Password }
}

export interface RegisterMemberSuccessAction {
  type: typeof REGISTER_MEMBER_S
  payload: { token: string }
}

export type SpotlightActionTypes =
  | SelectMemberInSpotlightAction
  | SelectMemberInSpotlightSuccessAction
  | SelectLibraryInSpotlightAction
  | SelectLibraryInSpotlightSuccessAction
  | CreateLibraryAction
  | CreateLibrarySuccessAction
  | DeleteLibraryAction
  | DeleteLibrarySuccessAction
  | RegisterMemberAction
  | RegisterMemberSuccessAction
