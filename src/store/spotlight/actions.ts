import {
  SELECT_MEMBER_IN_SPOTLIGHT,
  SELECT_MEMBER_IN_SPOTLIGHT_S,
  SELECT_LIBRARY_IN_SPOTLIGHT,
  SELECT_LIBRARY_IN_SPOTLIGHT_S,
  CREATE_LIBRARY,
  CREATE_LIBRARY_S,
  DELETE_LIBRARY,
  DELETE_LIBRARY_S,
  REGISTER_MEMBER,
  REGISTER_MEMBER_S,
  SelectMemberInSpotlightAction,
  CreateLibrarySuccessAction,
  CreateLibraryAction,
  SelectMemberInSpotlightSuccessAction,
  SelectLibraryInSpotlightAction,
  SelectLibraryInSpotlightSuccessAction,
  DeleteLibraryAction,
  DeleteLibrarySuccessAction,
  RegisterMemberAction,
  RegisterMemberSuccessAction
} from "./types"

import { User, Library } from "../../model"
import { Password } from "../../util/password"

export function selectMemberInSpotlight(userSlug: string | null, entry?: User): SelectMemberInSpotlightAction {
  return {
    type: SELECT_MEMBER_IN_SPOTLIGHT,
    payload: { userSlug, entry }
  }
}

export function selectMemberInSpotlightSuccess(entry: User): SelectMemberInSpotlightSuccessAction {
  return {
    type: SELECT_MEMBER_IN_SPOTLIGHT_S,
    payload: { entry }
  }
}

export function selectLibraryInSpotlight(librarySlug: string | null, entry?: Library): SelectLibraryInSpotlightAction {
  return {
    type: SELECT_LIBRARY_IN_SPOTLIGHT,
    payload: { librarySlug, entry }
  }
}

export function selectLibraryInSpotlightSuccess(entry: Library): SelectLibraryInSpotlightSuccessAction {
  return {
    type: SELECT_LIBRARY_IN_SPOTLIGHT_S,
    payload: { entry }
  }
}

export function createLibrary(entry: Partial<Library>): CreateLibraryAction {
  return {
    type: CREATE_LIBRARY,
    payload: { entry }
  }
}

export function createLibrarySuccess(library: Library): CreateLibrarySuccessAction {
  return {
    type: CREATE_LIBRARY_S,
    payload: { entry: library }
  }
}

export function deleteLibrary(entry: Library): DeleteLibraryAction {
  return {
    type: DELETE_LIBRARY,
    payload: { entry }
  }
}

export function deleteLibrarySuccess(count: number): DeleteLibrarySuccessAction {
  return {
    type: DELETE_LIBRARY_S,
    payload: { count }
  }
}

export function registerMember(email: string, userName: string, password: Password): RegisterMemberAction {
  return {
    type: REGISTER_MEMBER,
    payload: { email, userName, password }
  }
}

export function registerMemberSuccess(token: string): RegisterMemberSuccessAction {
  return {
    type: REGISTER_MEMBER_S,
    payload: { token }
  }
}
