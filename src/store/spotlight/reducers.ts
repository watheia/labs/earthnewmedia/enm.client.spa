import { concat } from "lodash"

import {
  SpotlightState,
  SELECT_MEMBER_IN_SPOTLIGHT,
  SELECT_MEMBER_IN_SPOTLIGHT_S,
  SELECT_LIBRARY_IN_SPOTLIGHT,
  SELECT_LIBRARY_IN_SPOTLIGHT_S,
  CREATE_LIBRARY,
  CREATE_LIBRARY_S,
  SpotlightEntry,
  SpotlightActionTypes,
  DELETE_LIBRARY,
  DELETE_LIBRARY_S
} from "./types"
import { User, Library } from "../../model"

const initialState: SpotlightState = {
  member: SpotlightEntry.with<User>(),
  library: SpotlightEntry.with<Library>()
}

function appendLibrary(user: User | null, library: Library): User | null {
  if (user === null) return null
  return {
    ...user,
    libraries: user.libraries ? concat(user.libraries, library) : [library]
  }
}

function removeLibrary(user: User | null, entry: Library): User | null {
  if (user === null) return null
  return {
    ...user,
    libraries: user.libraries ? user.libraries.filter(lib => lib.id !== entry.id) : []
  }
}

export function spotlightReducer(state: SpotlightState = initialState, action: SpotlightActionTypes): SpotlightState {
  switch (action.type) {
    case SELECT_MEMBER_IN_SPOTLIGHT:
      return {
        member: SpotlightEntry.with<User>(action.payload.userSlug, action.payload.entry),
        library: state.library
      }
    case SELECT_MEMBER_IN_SPOTLIGHT_S:
      return {
        member: SpotlightEntry.with<User>(state.member.slug, action.payload.entry),
        library: state.library
      }
    case SELECT_LIBRARY_IN_SPOTLIGHT:
      return {
        member: state.member,
        library: SpotlightEntry.with<Library>(action.payload.librarySlug, action.payload.entry)
      }
    case SELECT_LIBRARY_IN_SPOTLIGHT_S:
      return {
        member: state.member,
        library: SpotlightEntry.with<Library>(state.library.slug, action.payload.entry)
      }
    case CREATE_LIBRARY:
      return state
    case CREATE_LIBRARY_S:
      return {
        member: SpotlightEntry.with<User>(state.member.slug, appendLibrary(state.member.entry, action.payload.entry)),
        library: state.library
      }
    case DELETE_LIBRARY:
      return {
        member: SpotlightEntry.with<User>(state.member.slug, removeLibrary(state.member.entry, action.payload.entry)),
        library: state.library
      }
    case DELETE_LIBRARY_S:
      return state
    default:
      return state
  }
}
