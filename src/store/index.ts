import { applyMiddleware, createStore, combineReducers, Store } from "redux"
import createSagaMiddleware from "redux-saga"

import { sysReducer } from "./sys/reducers"
import { authReducer } from "./auth/reducers"
import logger from "redux-logger"
import sagas from "./sagas"
import { spotlightReducer } from "./spotlight/reducers"

const rootReducer = combineReducers({
  spotlight: spotlightReducer,
  sys: sysReducer,
  auth: authReducer
})

export type RootState = ReturnType<typeof rootReducer>

export default function configureStore(): Store {
  const sagaMiddleware = createSagaMiddleware()
  const store = createStore(rootReducer, applyMiddleware(logger, sagaMiddleware))
  sagaMiddleware.run(sagas)

  // const action = type => store.dispatch({ type })
  return store
}
