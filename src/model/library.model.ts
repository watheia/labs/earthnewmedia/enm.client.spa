import { AccessLevel, LibraryType } from "./base.model"
import { LibraryItem } from "./library-item.model"

export type Library = {
  id: string
  userId: string
  slug: string
  name: string
  description: string
  libraryType: LibraryType
  access: AccessLevel
  createdDate: Date
  items: LibraryItem[]
}
