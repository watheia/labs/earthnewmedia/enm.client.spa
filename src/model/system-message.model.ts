enum MessageType {
  Error,
  Info
}

export class SystemMessage {
  type: MessageType
  message: string

  private constructor(type: MessageType, message: string) {
    this.type = type
    this.message = message
  }

  static error(message: string): SystemMessage {
    return new SystemMessage(MessageType.Error, message)
  }

  static info(message: string): SystemMessage {
    return new SystemMessage(MessageType.Info, message)
  }
}
