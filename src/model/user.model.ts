import { UserRole } from "./base.model"
import { Library } from "./library.model"

export type User = {
  id: string
  email: string
  slug: string
  userName: string
  roles: Set<UserRole>
  libraries?: Library[]
}
