export enum UserRole {
  ADMIN = "admin",
  SUPPORT = "support",
  MEMBER = "member"
}

export enum AccessLevel {
  Private = "Private",
  Public = "Public"
}

export enum LibraryType {
  Discussion = "Discussion",
  Album = "Album",
  Gallery = "Gallery"
}