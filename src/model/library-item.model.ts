export interface ContentMetadata {
  uri: string
  mimetype: string
}

export interface ArticleMetadata extends ContentMetadata {
  icon?: string
}

export interface ImageMetadata extends ContentMetadata {
  width?: number
  height?: number
}

export interface AudioMetadata extends ContentMetadata {
  duration?: number
}

export interface VideoMetadata extends ContentMetadata {
  duration?: number
  width?: number
  height?: number
}

export type LibraryItem = {
  id: string
  libraryId: string
  title: string
  description?: string
  createdDate: Date
  metadata: ArticleMetadata | ImageMetadata | AudioMetadata | VideoMetadata
}
